// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#include "MKL03Z4.h"                         // KL03 M0+ register definitions


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#define ADC_Chan     0x02                    // Connect POT wiper lead to FRDM-KL03Z board, J4-1
#define nSW2         (PTB->PDIR&(1<<0))      // Switch 1, negative logic (0=pressed, 1=not pressed)
#define nSW3         (PTB->PDIR&(1<<5))      // Switch 2, negative logic (0=pressed, 1=not pressed)


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint16_t adcin;                              // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(uint32_t delay);              // delay in multiples of 1ms
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);    // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint16_t ADC_Read(uint8_t x);                        // Analog-to-Digital Converter byte read: enter channel to read



// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off   

__asm("CPSIE i");                            // Enable all Interrupts

NVIC->ICPR[0] = (uint32_t)(1 << (SysTick_IRQn & 0x1F));// Clear SysTick Interrupt in NVIC Core register
NVIC->ISER[0] = (uint32_t)(1 << (SysTick_IRQn & 0x1F));// Set SysTick Interrupt in NVIC Core register 

SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=4800000;                       // Core Clock/1=48MHz, Period=100ms/(1/48000000Hz)=4800000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk     // Enable SysTick timer and set clock source to processor clock (core clock)
             |SysTick_CTRL_TICKINT_Msk);     // Enable SysTick Interrupt      


for(;;);                                     // forever loop, wait for interrupt
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Disable Interrupts                       
__asm("CPSID i");                            // Disable interrupts

// System Registers
SIM->COPC=0x00;                              // Disable COP watchdog
SIM->SCGC5|=SIM_SCGC5_PORTA_MASK;            // Enable clock gate for PORTA registers
SIM->SCGC5|=SIM_SCGC5_PORTB_MASK;            // Enable clock gate for PORTB registers
SIM->SCGC6|=SIM_SCGC6_ADC0_MASK;             // Enable clock gate for ADC registers
SIM->SCGC5|=SIM_SCGC5_LPUART0_MASK;          // Enable clock gate for LPUART0 registers
SIM->SCGC6|=SIM_SCGC6_TPM0_MASK;             // Enable clock gate for TPM1 registers
//SIM->SOPT2|=SIM_SOPT2_CLKOUTSEL(7);          // Enable CLKOUT pin for HIRC 48MHz
SIM->SOPT2|=SIM_SOPT2_TPMSRC(1);             // Enable HIRC 48MHz CLK for TPM1 clock
SIM->SOPT2|=SIM_SOPT2_LPUART0SRC(1);         // Enable HIRC 48MHz CLK as the LPUART0 input clock
SIM->SCGC4|=SIM_SCGC4_CMP_MASK;              // Enable CMP0 module  

// Errata 8068 fix
SIM->SCGC6 |= SIM_SCGC6_RTC_MASK;            // enable clock to RTC   
RTC->TSR = 0x00;                             // dummy write to RTC TSR per errata 8068             
SIM->SCGC6 &= ~SIM_SCGC6_RTC_MASK;           // disable clock to RTC

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=48000;                         // Core Clock/1=48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock) 

// ADC Configuration
ADC0->CFG1=(ADC_CFG1_ADIV(2)                 // Set ADC input clock /4
           |ADC_CFG1_MODE(1)                 // Set ADC conversion mode to 12-bit
           |ADC_CFG1_ADICLK(0));             // Set ADC input to BUS CLK

// System clock initialization
MCG_MC|=MCG_MC_HIRCEN_MASK;                  // Enable 48MHz HIRC
MCG_C1=MCG_C1_CLKS(0);                       // Enable Main Clock Source of the 48MHz HIRC
SIM_CLKDIV1&=~(SIM_CLKDIV1_OUTDIV1(1));      // Set Core Clock/1=48MHz 
SIM_CLKDIV1|=(SIM_CLKDIV1_OUTDIV4(1));       // Set Bus Clock/2=24MHz    

// Timer0 Init
TPM0->SC=0x00;                               // busclk/1=20.80ns per count; buzzer on=0x08, off=0x00
TPM0->CONTROLS[1].CnSC=0x28;                 // edge-aligned PWM, Output on FRDM-KL03Z Board on J2-3
TPM0->MOD=48000;                             // 1KHz by default
TPM0->CONTROLS[1].CnV=48000>>1;              // half of the above to produce 50% duty cycle PWM

// GPIO Init
PORTB->PCR[10] = PORT_PCR_MUX(1);            // Set pin MUX for GPIO on PORTB pin 10
PORTB->PCR[11] = PORT_PCR_MUX(1);            // Set pin MUX for GPIO on PORTB pin 11
PORTB->PCR[13] = PORT_PCR_MUX(1);            // Set pin MUX for GPIO on PORTB pin 13
PORTA->PCR[9] = PORT_PCR_MUX(0);             // Set pin MUX for ADC0_SE2 on PORTA pin 9
PORTA->PCR[5] = PORT_PCR_MUX(2);             // Set pin MUX for TPM0CH1 on PORTA pin 5
PORTB->PCR[1] = PORT_PCR_MUX(2);             // Set pin MUX for LPUART0 TX on PORTB pin 1
PORTB->PCR[2] = PORT_PCR_MUX(2);             // Set pin MUX for LPUART0 RX on PORTB pin 2
PORTA->PCR[12] = PORT_PCR_MUX(0);            // Set pin MUX for CMP0_IN0 on PORTA pin 12
PORTB->PCR[0]|=PORT_PCR_MUX(1)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;  // SW2 is now a GPIO, with pull up enabled (negative logic)
PORTB->PCR[5]|=PORT_PCR_MUX(1)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;  // SW3 is now a GPIO, with pull up enabled (negative logic), Disable NMI Pin (PTB5) if using PTB5 for switches
PTB->PDDR|=(1<<10);                          // Set pin as output for Red LED
PTB->PDDR|=(1<<11);                          // Set pin as output for Green LED
PTB->PDDR|=(1<<13);                          // Set pin as output for Blue LED
	 
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue)   // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) PTB->PCOR|=(1<<10);  else PTB->PSOR|=(1<<10);
if (Green ==1) PTB->PCOR|=(1<<11);  else PTB->PSOR|=(1<<11);
if (Blue  ==1) PTB->PCOR|=(1<<13);  else PTB->PSOR|=(1<<13);
}
//---------------------------------------------------------------------
uint16_t ADC_Read(uint8_t x)                 // Analog-to-Digital Converter byte read: enter channel to read
{ADC0->SC1[0]=ADC_SC1_ADCH(x); while ((ADC0->SC1[0]&ADC_SC1_COCO_MASK)==0); 
return ADC0->R[0];
}
//---------------------------------------------------------------------


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void SysTick_Handler (void)
{
  SysTick->CTRL |= SysTick_CTRL_COUNTFLAG_Msk;  // Clear SysTick ISR interrupt flag
  GPIOB_PTOR|=(1<<11);                       // Toggle GREEN LED
}




