// INCLUDES /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#include "MKL43Z4.h"                         // KL43 M0+ register definitions


// DEFINES //////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#define nSW1         (GPIOA_PDIR&(1<<4))     // Switch 1, negative logic (0=pressed, 1=not pressed)
#define nSW3         (GPIOC_PDIR&(1<<3))     // Switch 3, negative logic (0=pressed, 1=not pressed)
#define ADC_Chan     0x08                    // Connect POT wiper lead to FRDM-KL43Z board, J4-2


// GLOBAL CONSTANTS /////////////////////////////////////////////////
//-------------------------------------------------------------------


// GLOBAL VARIABLES /////////////////////////////////////////////////
//-------------------------------------------------------------------
uint16_t adcin;                              // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////
//-------------------------------------------------------------------
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(uint32_t delay);              // delay in multiples of 1ms

//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green);         // RG-LED Control: 1=on, 0=off, for each of the 2 colors
uint16_t ADC_Read(uint8_t chan);             // Analog-to-Digital Converter int read: enter channel to read


// *** MAIN *********************************************************
//-------------------------------------------------------------------
int main(void)
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0);                                    // Start with all LEDs off
for(;;)                                      // forever loop
  {
   RGB(1,0);                                 // Turn Red LED On
   MCU_Delay(100);                           // Delay 100ms
   RGB(0,0);                                 // Turn Red LED Off
   MCU_Delay(100);                           // Delay 100ms
  }
// return 0;
}
//-------------------------------------------------------------------
// ******************************************************************


// FUNCTION BODIES //////////////////////////////////////////////////
//-------------------------------------------------------------------
void MCU_Init(void)
{
// Crucial
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
SIM_COPC=0x00;                               // Disable COP watchdog

//System Registers
SIM_SCGC5|=SIM_SCGC5_PORTA_MASK;             // Enable PortA clock
SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;             // Enable PortB clock
SIM_SCGC5|=SIM_SCGC5_PORTC_MASK;             // Enable PortC clock
SIM_SCGC5|=SIM_SCGC5_PORTD_MASK;             // Enable PortD clock
SIM_SCGC5|=SIM_SCGC5_PORTE_MASK;             // Enable PortE clock
SIM_SCGC5|=SIM_SCGC5_LPUART0_MASK;           // Enable LPUART0 module
SIM_SOPT2|=SIM_SOPT2_LPUART0SRC(1);          // MCGPCLK 48MHz clock used for LPUART0 clock
SIM_SCGC6|=SIM_SCGC6_PIT_MASK;               // Enable PIT clock
SIM_SCGC6|=SIM_SCGC6_ADC0_MASK;              // Enable clock gate for ADC registers
SIM_SCGC4|=SIM_SCGC4_CMP0_MASK;              // Enable CMP0 module
SIM_SCGC6|=SIM_SCGC6_DAC0_MASK;              // Enable DAC0 module
SIM_SCGC6|=SIM_SCGC6_TPM0_MASK;              // Enable TPM0 module
SIM_SOPT2|=SIM_SOPT2_TPMSRC(1);              // TPM Input Clock

// System clock initialization
MCG_MC|=MCG_MC_HIRCEN_MASK;                  // Enable 48MHz HIRC
MCG_C1=MCG_C1_CLKS(0);                       // Enable Main Clock Source of the 48MHz HIRC
SIM_CLKDIV1&=~(SIM_CLKDIV1_OUTDIV1(1));      // Set Core Clock/1=48MHz
SIM_CLKDIV1|=(SIM_CLKDIV1_OUTDIV4(1));       // Set Bus Clock/2=24MHz

// System Tick initialization
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=48000;                         // Core Clock/1=48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock)

// GPIO Init
PORTA_PCR1 =PORT_PCR_MUX(2);                 // Set Pin A1 to alt LPUART0_RX function
PORTA_PCR2 =PORT_PCR_MUX(2);                 // Set Pin A2 to alt LPUART0_RX function
PORTB_PCR0 =PORT_PCR_MUX(0);                 // Set Pin B0 to A/D Channel 8 function
PORTE_PCR29=PORT_PCR_MUX(0);                 // Set Pin E29 to CMP0_IN5 function
PORTE_PCR30=PORT_PCR_MUX(0);                 // Set Pin E30 to DAC0_OUT function
PORTD_PCR2=PORT_PCR_MUX(4);                  // Set pin MUX for TPM0_CH2 on PORTD pin 2
PORTA->PCR[4]|=PORT_PCR_MUX(1)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;  // SW1 is now a GPIO, with pull up enabled (negative logic), Disable NMI Pin if using PTA4 for switches
PORTC->PCR[3]|=PORT_PCR_MUX(1)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;  // SW3 is now a GPIO, with pull up enabled (negative logic)
PORTE_PCR31=PORT_PCR_MUX(1);                 // Set Pin E31 to GPIO function
PORTD_PCR5=PORT_PCR_MUX(1);                  // Set Pin D5 to GPIO function
GPIOE_PDDR|=(uint32_t)(1<<31);               // Set Pin E31 as output (RED LED)
GPIOD_PDDR|=(1<<5);                          // Set Pin D5 as output (GREEN LED)
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green)          // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOE_PCOR|=(uint32_t)(1<<31);  else GPIOE_PSOR|=(uint32_t)(1<<31);
if (Green ==1) GPIOD_PCOR|=(1<<5);             else GPIOD_PSOR|=(1<<5);
}
//---------------------------------------------------------------------
uint16_t ADC_Read(uint8_t chan)              // Analog-to-Digital Converter int read: enter channel to read
{ADC0_SC1A=chan; while (!(ADC0_SC1A&ADC_SC1_COCO_MASK)); return ADC0_RA;}


// INTERRUPT BODIES /////////////////////////////////////////////////
//-------------------------------------------------------------------


