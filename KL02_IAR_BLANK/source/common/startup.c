/*
 * File:    startup.c
 * Purpose: Generic Kinetis startup code
 *
 * Notes:
 */

// function prototypes
void sysinit (void);
void common_startup(void);


#include "arm_cm0.h"
#include "MKL02Z4.h"


#if (defined(IAR))
	#pragma section = ".data"
	#pragma section = ".data_init"
	#pragma section = ".bss"
	#pragma section = "CodeRelocate"
	#pragma section = "CodeRelocateRam"
#endif

/********************************************************************/
/*!
 * \brief   Kinetis L Family Start
 * \return  None
 *
 * This function calls all of the necessary startup routines and then 
 * branches to the main process.
 */

void start(void)
{ 
 
	/* Disable the watchdog timer */
        
        SIM_COPC = 0x00;

	/* Copy any vector or data sections that need to be in RAM */
	common_startup();

	/* Perform clock initialization, default UART initialization,
         * initializes clock out function, and enables the abort button
         */
	
              
        /* Enable all of the port clocks. These have to be enabled to configure
         * pin muxing options, so most code will need all of these on anyway.
         */
        SIM_SCGC5 |= (SIM_SCGC5_PORTA_MASK
                      | SIM_SCGC5_PORTB_MASK);
         
//if ((RCM_SRS0 & RCM_SRS0_WAKEUP_MASK)&&((SMC_PMCTRL & SMC_PMCTRL_STOPM_MASK)== 4)&&((SMC_STOPCTRL & SMC_STOPCTRL_VLLSM_MASK)== 1))
       
        
        // Release hold with ACKISO:  Only has an effect if recovering from VLLS1, VLLS2, or VLLS3
        // if ACKISO is set you must clear ackiso before calling pll_init 
        //    or pll init hangs waiting for OSC to initialize
        // if osc enabled in low power modes - enable it first before ack
        // if I/O needs to be maintained without glitches enable outputs and modules first before ack.
        if (PMC_REGSC &  PMC_REGSC_ACKISO_MASK)
        PMC_REGSC |= PMC_REGSC_ACKISO_MASK;
      
       /* Ramp up the system clock */
       /* Set the system dividers */
        SIM_CLKDIV1 = ( 0
                        | SIM_CLKDIV1_OUTDIV1(0)
                        | SIM_CLKDIV1_OUTDIV4(1) );
        
        
        
	/* Jump to main process */
	main();
}






/********************************************************************/
void common_startup(void)
{ 
    /* Declare a counter we'll use in all of the copy loops */
    uint32 n;
 
 
    /* Addresses for VECTOR_TABLE and VECTOR_RAM come from the linker file */  
    extern uint32 __VECTOR_TABLE[];
    extern uint32 __VECTOR_RAM[];

    /* Get the addresses for the .data section (initialized data section) */
    uint8* data_ram = __section_begin(".data");
    uint8* data_rom = __section_begin(".data_init");
    uint8* data_rom_end = __section_end(".data_init");
    
    /* Copy initialized data from ROM to RAM */
    n = data_rom_end - data_rom;
    while (n--)
      *data_ram++ = *data_rom++;
 
 
    /* Get the addresses for the .bss section (zero-initialized data) */
    uint8* bss_start = __section_begin(".bss");
    uint8* bss_end = __section_end(".bss");
    
    /* Clear the zero-initialized data section */
    n = bss_end - bss_start;
    while(n--)
      *bss_start++ = 0;    

    /* Get addresses for any code sections that need to be copied from ROM to RAM.
     * The IAR tools have a predefined keyword that can be used to mark individual
     * functions for execution from RAM. Add "__ramfunc" before the return type in
     * the function prototype for any routines you need to execute from RAM instead
     * of ROM. ex: __ramfunc void foo(void);
     */
    #if (defined(IAR))
            uint8* code_relocate_ram = __section_begin("CodeRelocateRam");
            uint8* code_relocate = __section_begin("CodeRelocate");
            uint8* code_relocate_end = __section_end("CodeRelocate");

            /* Copy functions from ROM to RAM */
            n = code_relocate_end - code_relocate;
            while (n--)
                    *code_relocate_ram++ = *code_relocate++;
    #endif

}
/********************************************************************/
