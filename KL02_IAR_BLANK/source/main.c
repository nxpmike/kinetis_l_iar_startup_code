// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#include "MKL02Z4.h"                         // KL02 M0+ register definitions


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#define Pot          0x02                    // Potentiometer is on A/D 0 Channel 2


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint32_t adcin;                              // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(uint32_t delay);              // delay in multiples of 1ms

//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);        // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint32_t ADC_Read(uint8_t chan);                     // Analog-to-Digital Converter byte read: enter channel to read


// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off
for(;;)                                      // forever loop 
  { 
   RGB(1,0,0);                               // Turn Red LED On
   MCU_Delay(100);                           // Delay 100ms
   RGB(0,0,0);                               // Turn Red LED Off
   MCU_Delay(100);                           // Delay 100ms
  } 
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Crucial                       
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
SIM_COPC=0x00;                               // Disable COP watchdog

//System Registers 
SIM_CLKDIV1 = 0x00;                          // Set Busclk to /1, Busclk=20,971,520Hz
SIM_SOPT2=0x05000000;                        // TPM Input Clock, UART0 Clock is MCGFLLCLK 
SIM_SCGC5|=SIM_SCGC5_PORTA_MASK;             // Enable PortA clock
SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;             // Enable PortB clock
SIM_SCGC6|=SIM_SCGC6_TPM1_MASK;              // Enable TPM1 module   
SIM_SCGC6|=SIM_SCGC6_ADC0_MASK;              // Enable ADC module
SIM_SCGC4|=SIM_SCGC4_UART0_MASK;             // Enable UART0 module
SIM_SCGC4|=SIM_SCGC4_I2C0_MASK;              // Enable I2C0 Module
SIM_SCGC4|=SIM_SCGC4_CMP_MASK;               // Enable CMP0 module  

//System clock initialization
MCG_C1=0x04;                                 // FEI mode, internal clock 32KHz
MCG_C2=0x80;                                 // LOC reset enable, slow ref clock select

// System Tick Init
#if defined(IAR)
SYST_CSR=0x00000000;                         // Disable the SysTick Timer
SYST_RVR=SysTick_RVR_RELOAD(20972);          // Busclk/1=32.768KHz*640=20971520Hz, Period=1ms/(1/20971520Hz)=20972
SYST_CVR=SysTick_CVR_CURRENT(0x00);          // Clear the current counter value to 0
SYST_CSR=(SysTick_CSR_ENABLE_MASK
           |SysTick_CSR_CLKSOURCE_MASK);     // Enable SYS TICK timer and set clock source to processor clock (core clock)
#else
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=20972;                         // Core Clock/1=48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock)
#endif

// ADC Init
ADC0_CFG2=0x00;                              // uint32_t sample time, ADC0A channels selected 
ADC0_CFG1=0x40;                              // mode 8-bit(0x40), 10-bit(0x48), 12-bit(0x44), busclk/4 = 5.24MHz
ADC0_SC2=0x00;                               // DMA disabled, VREH & VREF pins selected
ADC0_SC3=0x00;                               // Single Conversion, Continuous Converstion is (0x08)
ADC0_SC1A=0x02;                              // Set to ADC0 channel 2, if Continous Converstion mode selected, ADC0_SC3 MUST BE WRITTEN TO LAST

// I2C Init
I2C0_F=0x11;                                 // I2C Baudrate=20971520Hz/(1*104)=201kbps
I2C0_C1|=I2C_C1_IICEN_MASK;                  // I2C on

// Timer2 Init
TPM1_SC=0x00;                                // busclk/1=47.68ns per count; buzzer on=0x08, off=0x00
TPM1_C1SC=0x28;                              // edge-aligned PWM
TPM1_MOD=20970-1;                            // 1kHz by default
TPM1_C1V=20970>>1;                           // half of the above to produce 50% duty cycle PWM 

// GPIO Init
PORTA_PCR9=PORT_PCR_MUX(0);                  // Set Pin A9 to ADC0_SE2 function
PORTB_PCR6=PORT_PCR_MUX(1);                  // Set Pin B6 to GPIO function
PORTB_PCR7=PORT_PCR_MUX(1);                  // Set Pin B7 to GPIO function
PORTB_PCR10=PORT_PCR_MUX(1);                 // Set Pin B10 to GPIO function
PORTB_PCR5=PORT_PCR_MUX(2);                  // Set Pin B5 to TPM1 CH1 function
PORTB_PCR1=PORT_PCR_MUX(2);                  // Set Pin B1 to alt UART0_TX function
PORTB_PCR2=PORT_PCR_MUX(2);                  // Set Pin B2 to alt UART0_RX function
PORTB_PCR3=PORT_PCR_MUX(2);                  // Set Pin B3 to alt I2C0_SCL function
PORTB_PCR4=PORT_PCR_MUX(2);                  // Set Pin B4 to alt I2C0_SDA function
PORTA_PCR12=PORT_PCR_MUX(0);                 // Set Pin A12 to alt CMP0_IN0 function - DEFAULT STATE
PORTA_PCR10=PORT_PCR_MUX(1)|0x03;            // ACCEL INT1 is now a GPIO, with pullup enabled
GPIOB_PDDR|=(1<<6);                          // Red LED, Negative Logic (0=on, 1=off)
GPIOB_PDDR|=(1<<7);                          // Green LED, Negative Logic (0=on, 1=off)
GPIOB_PDDR|=(1<<10);                         // Blue LED, Negative Logic (0=on, 1=off)

}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
#if defined(IAR)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SYST_CSR&SysTick_CSR_COUNTFLAG_MASK));}}
#else
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
#endif
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue)         // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB_PCOR|=(1<<6);  else GPIOB_PSOR|=(1<<6);
if (Green ==1) GPIOB_PCOR|=(1<<7);  else GPIOB_PSOR|=(1<<7);
if (Blue  ==1) GPIOB_PCOR|=(1<<10); else GPIOB_PSOR|=(1<<10);
}
//---------------------------------------------------------------------
uint32_t ADC_Read(uint8_t chan)                      // Analog-to-Digital Converter byte read: enter channel to read
{ADC0_SC1A=chan; while (!(ADC0_SC1A&ADC_SC1_COCO_MASK)); return ADC0_RA;}


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


