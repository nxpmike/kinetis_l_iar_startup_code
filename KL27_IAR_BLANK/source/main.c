// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#include "MKL27Z644.h"                       // KL27 M0+ register definitions


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#define nSW1         (GPIOA_PDIR&(1<<4))     // Switch 1, negative logic (0=pressed, 1=not pressed)
#define nSW3         (GPIOC_PDIR&(1<<1))     // Switch 3, negative logic (0=pressed, 1=not pressed)
#define ADC_Chan     0x01                    // Connect POT wiper lead to FRDM-KL27Z board, J4-2


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint16_t adcin;                              // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(uint32_t delay);              // delay in multiples of 1ms

//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);    // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint16_t ADC_Read(uint8_t chan);                     // Analog-to-Digital Converter int read: enter channel to read


// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off   

__asm("CPSIE i");                            // Enable all Interrupts

NVIC->ICPR[0] = (uint32_t)(1 << (SysTick_IRQn & 0x1F));// Clear SysTick Interrupt in NVIC Core register
NVIC->ISER[0] = (uint32_t)(1 << (SysTick_IRQn & 0x1F));// Set SysTick Interrupt in NVIC Core register 

SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=4800000;                       // Core Clock/1=48MHz, Period=100ms/(1/48000000Hz)=4800000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk     // Enable SysTick timer and set clock source to processor clock (core clock)
             |SysTick_CTRL_TICKINT_Msk);     // Enable SysTick Interrupt      


for(;;);                                     // forever loop, wait for interrupt
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Crucial                       
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
SIM_COPC=0x00;                               // Disable COP watchdog

//System Registers 
SIM_SCGC5|=SIM_SCGC5_PORTA_MASK;             // Enable PortA clock
SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;             // Enable PortB clock
SIM_SCGC5|=SIM_SCGC5_PORTC_MASK;             // Enable PortC clock
SIM_SCGC5|=SIM_SCGC5_PORTD_MASK;             // Enable PortD clock
SIM_SCGC5|=SIM_SCGC5_PORTE_MASK;             // Enable PortE clock
SIM_SCGC5|=SIM_SCGC5_LPUART0_MASK;           // Enable LPUART0 module   
SIM_SOPT2|=SIM_SOPT2_LPUART0SRC(1);          // MCGPCLK 48MHz clock used for LPUART0 clock 
SIM_SCGC6|=SIM_SCGC6_PIT_MASK;               // Enable PIT clock
SIM_SCGC5|=SIM_SCGC5_LPTMR_MASK;             // Enable LPTMR0 clock
SIM_SCGC6|=SIM_SCGC6_ADC0_MASK;              // Enable clock gate for ADC registers
SIM_SCGC4|=SIM_SCGC4_CMP0_MASK;              // Enable CMP0 module  
SIM_SCGC6|=SIM_SCGC6_TPM0_MASK;              // Enable TPM0 module 
SIM_SOPT2|=SIM_SOPT2_TPMSRC(1);              // TPM Input Clock

// System clock initialization
MCG_MC|=MCG_MC_HIRCEN_MASK;                  // Enable 48MHz HIRC
MCG_C1=MCG_C1_CLKS(0);                       // Enable Main Clock Source of the 48MHz HIRC
SIM_CLKDIV1&=~(SIM_CLKDIV1_OUTDIV1(1));      // Set Core Clock/1=48MHz 
SIM_CLKDIV1|=(SIM_CLKDIV1_OUTDIV4(1));       // Set Bus Clock/2=24MHz    

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=48000;                         // Core Clock/1=48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock) 

// GPIO Init
PORTA_PCR1=PORT_PCR_MUX(2);                  // Set Pin A1 to alt LPUART0_RX function
PORTA_PCR2=PORT_PCR_MUX(2);                  // Set Pin A2 to alt LPUART0_RX function
PORTE_PCR16=PORT_PCR_MUX(0);                 // Set pin MUX for ADC0_SE1 on PORTE pin 16
PORTC_PCR9=PORT_PCR_MUX(3);                  // Set pin MUX for TPM0_CH5 on PORTC pin 9
PORTC_PCR8=PORT_PCR_MUX(0);                  // Set pin MUX for CPM0_IN2
PORTA->PCR[4]|=PORT_PCR_MUX(1)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;  // SW1 is now a GPIO, with pull up enabled (negative logic), Disable NMI Pin if using PTA4 for switches
PORTC->PCR[1]|=PORT_PCR_MUX(1)|PORT_PCR_PE_MASK|PORT_PCR_PS_MASK;  // SW3 is now a GPIO, with pull up enabled (negative logic)
PORTB_PCR18=PORT_PCR_MUX(1);                 // Set Pin B18 to GPIO function
PORTB_PCR19=PORT_PCR_MUX(1);                 // Set Pin B19 to GPIO function
PORTA_PCR13=PORT_PCR_MUX(1);                 // Set Pin A13 to GPIO function
GPIOB_PDDR|=(1<<18);                         // Set Pin B18 as output
GPIOB_PDDR|=(1<<19);                         // Set Pin B19 as output
GPIOA_PDDR|=(1<<13);                         // Set Pin A13 as output
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue) // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB_PCOR|=(1<<18); else GPIOB_PSOR|=(1<<18);
if (Green ==1) GPIOB_PCOR|=(1<<19); else GPIOB_PSOR|=(1<<19);
if (Blue  ==1) GPIOA_PCOR|=(1<<13); else GPIOA_PSOR|=(1<<13);
}
//---------------------------------------------------------------------
uint16_t ADC_Read(uint8_t chan)              // Analog-to-Digital Converter int read: enter channel to read
{ADC0_SC1A=chan; while (!(ADC0_SC1A&ADC_SC1_COCO_MASK)); return ADC0_RA;}


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void SysTick_Handler (void)
{
  SysTick->CTRL |= SysTick_CTRL_COUNTFLAG_Msk;  // Clear SysTick ISR interrupt flag
  GPIOB_PTOR|=(1<<18);                       // Toggle GREEN LED
}



